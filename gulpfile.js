"use strict";

// required packages
const gulp = require("gulp"),
		inline = require("gulp-inline"), // markup - image SVGs to inline SVGs
		sass = require("gulp-sass"), // style - SCSS to CSS
		autoprefixer = require("gulp-autoprefixer"), // style - adds vendor prefixes
		babel = require("gulp-babel"), // script - ES6 to ES5 (requires "babel-preset-es2015" module)
		plumber = require("gulp-plumber"), // build tool - prevents gulp pipe from stopping on error
		notify = require("gulp-notify"), // build tool - used to show errors/messages via tray notifications
		sourcemaps = require("gulp-sourcemaps"), // build tool - creates sourcemaps for source file
		browsersync = require("browser-sync"); // build tool - synchronize view/scroll states in multiple browsers

// default tasks to run on "gulp" command
gulp.task("default",
	[
		"style",
		"script",
		"sync",
		"watch"
	],
	() => gulp.src("").pipe(notify({message: "Default tasks completed."}))
);

// style task
// 1) source: all SCSS from dev/styles/ pipes into...
// 2) plumber: prevents gulp pipe from stopping on error and calls notify if an error is found
// 3) notify: shows tray notification listing error message and line number, then pipes into...
// 4) sourcemaps: initializes sourcemaps for source file, then pipes into...
// 5) sass: converts SCSS to compressed/minified CSS
// 6) if sass finds a syntax error it logs it in the terminal, then pipes into...
// 7) autoprefixer: adds in vendor prefixes, then pipes into...
// 8) sourcemaps: writes sourcemaps for source file, then pipes into...
// 9) destination: public/styles/ which then pipes into...
// 10) browsersync: injects CSS into the browser without full page reload
gulp.task("style", () => {
	return gulp.src("sass/style.scss")
		.pipe(plumber({
			errorHandler: notify.onError("STYLE ERROR: <%= error.message %>")
		}))
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: "compressed"})
			.on("error", sass.logError))
		.pipe(autoprefixer({
			browsers: ["last 5 versions"],
			cascade: false
		}))
		.pipe(sourcemaps.write("./"))
		.pipe(gulp.dest("./"))
		.pipe(browsersync.stream({match: "**/*.css"}));
});

// script task
// 1) source: all JS from es6/ pipes into...
// 2) plumber: prevents gulp pipe from stopping on error and calls notify if an error is found
// 3) notify: shows tray notification listing error message and line number, then pipes into...
// 4) sourcemaps: initializes sourcemaps for source file, then pipes into...
// 5) babel: transpiles ES6 to ES5 using the ES2015 preset, compacts and minifies code, then pipes into...
// 6) sourcemaps: writes sourcemaps for source file, then pipes into...
// 7) destination: js/ which then pipes into...
// 8) browsersync: reload the page with the updated script
gulp.task("script", () => {
	return gulp.src("es6/*.js")
		.pipe(plumber({
			errorHandler: notify.onError("SCRIPT ERROR: <%= error.message %>")
		}))
		.pipe(sourcemaps.init())
		.pipe(babel({
			presets: ["es2015"],
			compact: true,
			minified: true
		}))
		.pipe(sourcemaps.write("./"))
		.pipe(gulp.dest("js/"))
		.pipe(browsersync.reload({stream: true}));
});

// sync task
// 1) initialize browsersync
// 2) browser(s) in which to open HTML document
// 3) serve files via proxy
gulp.task("sync", () => {
	browsersync.init({
		browser: ["chrome", "firefox", "iexplore"],
		proxy: "http://localhost/wp-dustin-ruetz"
	});
});

// watch task
gulp.task("watch", () => {
	// files to watch and tasks to run
	gulp.watch("**/*.php", browsersync.reload);
	gulp.watch("sass/**/*.scss", ["style"]);
	gulp.watch("es6/*.js", ["script"]);
});