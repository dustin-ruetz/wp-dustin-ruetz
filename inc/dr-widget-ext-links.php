<?php
/**
 * Create a custom widget for external links.
 * Gets all entries from "Links" menu item and renders an HTML list
 * @link https://codex.wordpress.org/Widgets_API
 * @package dr_theme
 */

	class dr_Widget_Ext_Links extends WP_Widget {
		// define widget's name, description, etc.
		public function __construct() {
			$widget_ops = array( 
				'classname' => 'dr_Widget_Ext_Links',
				'description' => 'Custom widget for external links.',
			);
			parent::__construct( 'dr_Widget_Ext_Links', 'dr_Widget_Ext_Links', $widget_ops );
		}

		/**
		 * Outputs the content of the widget
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {
			// output the content of the widget
?>

	<ul class="dr-widget ext-links">
		<?php
			// get entries from "Links" menu item
			$links = get_bookmarks();

			// print $links array to <pre> element
			// echo "<pre>", print_r($links, 1), "</pre>";

			for ($i = 0; $i < count($links); $i++) {
		?>
		<li>
			<a
				class="ext-link <?php echo strtolower($links[$i]->link_name); ?>"
				href="<?php echo $links[$i]->link_url; ?>"
				target="_blank"
			>
				<figure>
					<img
						class="style-svg"
						src="<?php echo get_home_url() ?><?php echo $links[$i]->link_image; ?>"
					/>
					<figcaption>
						<?php echo $links[$i]->link_name; ?>
						<span class="hidden">link</span>
					</figcaption>
				</figure>
			</a>
		</li>
		<?php
			}
		?>
	</ul> <!-- /.dr-widget.ext-links -->

<?php
		}

		/**
		 * Output the options form on admin
		 * @param array $instance The widget options
		 */
		public function form( $instance ) {
			// output the options form on admin
		}

		/**
		 * Processing widget options on save
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 */
		public function update( $new_instance, $old_instance ) {
			// processes widget options to be saved
		}
	}
?>