"use strict";// replace drUnderscoresFork with theme name
var drUnderscoresFork={localDevEnv:undefined,// i.e. local development environment
// shorthand helper methods that implicitly return
// the querySelector and querySelectorAll methods
dqs:function dqs(query){return document.querySelector(query)},dqsa:function dqsa(query){return document.querySelectorAll(query)}};drUnderscoresFork.init=function(){drUnderscoresFork.checkEnv()};// check environment, i.e. check if URL contains "localhost"
drUnderscoresFork.checkEnv=function(){if(window.location.href.indexOf("localhost")>-1){drUnderscoresFork.localDevEnv=true;drUnderscoresFork.getDevHelperFiles()}else{drUnderscoresFork.localDevEnv=false};};// add CSS and JS dev helper files to the page
drUnderscoresFork.getDevHelperFiles=function(){var wdHelperCSS=document.createElement("link");wdHelperCSS.setAttribute("rel","stylesheet");wdHelperCSS.setAttribute("href","http://localhost:4000/public/styles/window-dimensions-helper.css");drUnderscoresFork.dqs("head").appendChild(wdHelperCSS);var wdHelperJS=document.createElement("script");wdHelperJS.setAttribute("src","http://localhost:4000/public/scripts/window-dimensions-helper.js");drUnderscoresFork.dqs("body").appendChild(wdHelperJS);window.setTimeout(function(){wdHelper.init()},500);// delay initialization by 500ms to allow for loading time
};document.addEventListener("DOMContentLoaded",function(){return drUnderscoresFork.init()});
//# sourceMappingURL=main.js.map
