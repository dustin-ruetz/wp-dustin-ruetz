# wp-dustin-ruetz

Theme for dustinruetz.com WordPress site.

Based on a [customized fork][dr-underscores-fork] of the [_s (underscores) WordPress theme][underscores-theme] by [Automattic][automattic].

[dr-underscores-fork]: https://github.com/dustin-ruetz/_s
[underscores-theme]: https://github.com/Automattic/_s
[automattic]: https://automattic.com