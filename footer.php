<?php
/**
 * Template to end the page.
 * Begins with <div#content> closing tag and subsequently closes all other HTML elements.
 * @link https://developer.wordpress.org/themes/basics/template-files#template-partials
 * @package DR_Theme
 */
?>

			</div> <!-- /#content -->

			<footer id="colophon" class="site-footer" role="contentinfo">
				<div class="site-info">
					<?php the_widget("dr_Widget_Ext_Links") ?>
				</div> <!-- /.site-info -->
			</footer> <!-- /#colophon -->
		</div> <!-- /#page -->
	<?php wp_footer(); ?>
	</body>
</html>