"use strict";

// replace drUnderscoresFork with theme name
const drUnderscoresFork = {
	localDevEnv: undefined, // i.e. local development environment

	// shorthand helper methods that implicitly return
	// the querySelector and querySelectorAll methods
	dqs: (query) => document.querySelector(query),
	dqsa: (query) => document.querySelectorAll(query)
};

drUnderscoresFork.init = () => {
	drUnderscoresFork.checkEnv();
};

// check environment, i.e. check if URL contains "localhost"
drUnderscoresFork.checkEnv = () => {
	if(window.location.href.indexOf("localhost") > -1) {
		drUnderscoresFork.localDevEnv = true;
		drUnderscoresFork.getDevHelperFiles();
	} else {
		drUnderscoresFork.localDevEnv = false;
	};
};

// add CSS and JS dev helper files to the page
drUnderscoresFork.getDevHelperFiles = () => {
	const wdHelperCSS = document.createElement("link");
	wdHelperCSS.setAttribute("rel", "stylesheet");
	wdHelperCSS.setAttribute("href", "http://localhost:4000/public/styles/window-dimensions-helper.css");
	drUnderscoresFork.dqs("head").appendChild(wdHelperCSS);

	const wdHelperJS = document.createElement("script");
	wdHelperJS.setAttribute("src", "http://localhost:4000/public/scripts/window-dimensions-helper.js");
	drUnderscoresFork.dqs("body").appendChild(wdHelperJS);

	window.setTimeout(() => {
		wdHelper.init();
	}, 500); // delay initialization by 500ms to allow for loading time
};

document.addEventListener( "DOMContentLoaded", () => drUnderscoresFork.init() );